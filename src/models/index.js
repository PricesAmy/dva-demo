/**
 * 取出当前目录下的js文件，并过滤掉index.js文件，再用map遍历文件名，context返回default方法：
 */
const context = require.context('./', false, /\.js$/);
export default context
  .keys()
  .filter(item => item !== './index.js')
  .map(key => context(key));
