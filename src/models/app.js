import { routerRedux } from 'dva/router';

export default {
  namespace: 'app',
  effects: {
    // 路由跳转
    *redirect({ payload }, { put }) {
      yield put(routerRedux.push('/user', { name: 'dkvirus', age: 20 }));
    },
  },
  reducers: {},
};
