import React from "react";
import { Route, Switch, routerRedux } from "dva/router";
// antd国际化
import { LocaleProvider } from "antd";
import en_US from "antd/lib/locale-provider/en_US";
import zh_CN from "antd/lib/locale-provider/zh_CN";
import "moment/locale/zh-cn";

// 主入口挂载props
import Layout from "./layout";

import IndexPage from "./routes/IndexPage";

/**
Route 为 react-router-dom 内的标签
ConnectedRouter 为 react-router-redux 内的对象 routerRedux 的标签，作用相当于 react-router-dom 中的 BrowserRouter 标签，作用为连接 redux 使用。
 */
const { ConnectedRouter } = routerRedux;

function RouterConfig({ history }) {
  const locale = localStorage.getItem("lang") !== "zh" ? zh_CN : en_US || zh_CN;
  return (
    <LocaleProvider locale={locale}>
      <ConnectedRouter history={history}>
        <Layout>
          <Switch>
            <Route path="/" exact component={IndexPage} />
            <Route path="/product" exact component={IndexPage} />
          </Switch>
        </Layout>
      </ConnectedRouter>
    </LocaleProvider>
  );
}

export default RouterConfig;
