import dva from "dva";
// 用于处理 loading 状态的
import createLoading from "dva-loading";
// redux日志
import { createLogger } from "redux-logger";

import { message } from "antd";

import "./index.css";

// 消除#
import { createBrowserHistory as createHistory } from "history";

// 1. Initialize
const app = dva({
  history: createHistory(),
  onError(e) {
    message.error(e.message);
  },
  onAction: createLogger({
    log: "info"
  })
});

// 2. Plugins
app.use(createLoading({ effects: true }));

// 3. Model
// require('./models').default.forEach(key => {
//   app.model(key.default);
// });

// 4. Router
app.router(require("./router").default);

// 5. Start
app.start("#root");
