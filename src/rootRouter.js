import React from "react";
import { Switch, routerRedux } from "dva/router";
import Layout from "./layout";

import Routes from "./routesAll";

/**
Route 为 react-router-dom 内的标签
ConnectedRouter 为 react-router-redux 内的对象 routerRedux 的标签，作用相当于 react-router-dom 中的 BrowserRouter 标签，作用为连接 redux 使用。
 */
const { ConnectedRouter } = routerRedux;

function RouterConfig({ history, app }) {
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Switch>{Routes(app)}</Switch>
      </Layout>
    </ConnectedRouter>
  );
}

export default RouterConfig;
