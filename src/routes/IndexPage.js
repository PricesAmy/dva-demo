import React from 'react';
import { connect } from 'dva';
import { Button, Select } from 'antd';
import './IndexPage.less';
// const { Option } = Select;

const prefix = 'layout';

function IndexPage () {
  return (
    <div className={ `${ prefix }-normal` }>
      <h1 className={ `${ prefix }-title` }>Yay! Welcome to dva!</h1>
      <ul>
        <Button>按钮</Button>
        <Select defaultValue="lucy" style={ { width: 120 } }>
          {/* <Option>sdd</Option> */}
        </Select>
      </ul>
    </div>
  );
}

IndexPage.propTypes = {};

export default connect()(IndexPage);
