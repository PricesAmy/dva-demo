export default {
  define: {
    'process.env': {},
    'process.env.NODE_ENV': process.env.NODE_ENV,
    'process.env.API_ENV': process.env.API_ENV
  },
  publicPath: '/',
  outputPath: './dist',
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true
      }
    ]
  ],
  theme: './src/theme.js',
  disableCSSModules: true, // 关闭CSS Modules
  proxy: {
    '/api': {
      target: 'http://your-api-server',
      changeOrigin: true
    }
  },
  env: {
    development: {
      extraBabelPlugins: [
        'dva-hmr',
        [
          'module-resolver',
          {
            root: ['./src'],
            alias: {
              utils: '/utils',
              underscore: 'lodash'
            }
          }
        ]
      ]
    }
  }
};
